// console.log('Hello World');

const cubeRoot = (x,y) => x ** y;

let x = parseInt(prompt("Please enter a number"));
const y = 3;
let getCube = cubeRoot (x,y);
console.log(`The cube of ${x} is ${getCube}`);

let address = ["258", "Washington Ave", "NW", "California", "90011"];

const [houseNum, streetName, county, state, zipcode ] = address;
console.log(`I live at ${houseNum} ${streetName} ${county} ${state} ${zipcode}`)

const animal = {
	name: "Lolong",
	breed: "Saltwater Crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in"
}
console.log(`${animal.name} was a ${animal.breed}. He weighed at ${animal.weight} with a measurement of ${animal.length}`)

const number = [1, 2, 3, 4, 5];
number.forEach((num) => console.log(num))

const reduceNumber = number.reduce((a,b) => { return a+b } )
console.log(reduceNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const myDog = new Dog ("Frankie" ,5, "Miniature Dachshund");
console.log(myDog);